/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamenc01;

/**
 *
 * @author Edgar Guerrero
 */
public class Perecederos extends Producto {

    private String temperatura, fechaCaducidad;

    public Perecederos(String temperatura, String fechaCaducidad) {
        this.temperatura = temperatura;
        this.fechaCaducidad = fechaCaducidad;
    }

    public Perecederos(String temperatura, String fechaCaducidad, int id, int unidad, float precio, String nombre) {
        super(id, unidad, precio, nombre);
        this.temperatura = temperatura;
        this.fechaCaducidad = fechaCaducidad;
    }

    public Perecederos() {
        this.temperatura = "";
        this.fechaCaducidad = "";
    }

    public String getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(String temperatura) {
        this.temperatura = temperatura;
    }

    public String getFechaCaducidad() {
        return fechaCaducidad;
    }

    public void setFechaCaducidad(String fechaCaducidad) {
        this.fechaCaducidad = fechaCaducidad;
    }

    @Override
    public float calcularPrecio() {
        float preciocalculado = 0.0f;
        switch (this.getUnidad()) {
            case 1:
                preciocalculado = this.precio * 1.03f;
                preciocalculado = preciocalculado * 1.5f;
                break;
            case 2:
                preciocalculado = this.precio * 1.05f;
                preciocalculado = preciocalculado * 1.5f;
                break;
            case 3:
                preciocalculado = this.precio * 1.04f;
                preciocalculado = preciocalculado * 1.5f;
                break;
        }
        return preciocalculado;
    }

}
