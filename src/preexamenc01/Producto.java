/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamenc01;

/**
 *
 * @author Edgar Guerrero
 */
public abstract class Producto {
    protected int id, unidad;
    protected float precio;
    protected String nombre;

    public Producto() {
        this.id = 0;
        this.unidad = 0;
        this.precio = 0.0f;
        this.nombre = "";
    }

    public Producto(int id, int unidad, float precio, String nombre) {
        this.id = id;
        this.unidad = unidad;
        this.precio = precio;
        this.nombre = nombre;
    }

    
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUnidad() {
        return unidad;
    }

    public void setUnidad(int unidad) {
        this.unidad = unidad;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    
    public abstract float calcularPrecio();
}
