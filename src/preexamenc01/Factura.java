/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamenc01;

/**
 *
 * @author Edgar Guerrero
 */
public class Factura extends Nota implements Impuesto {

    private String rfc, nombreCliente, domicilioFiscal;

    public Factura(String rfc, String nombreCliente, String domicilioFiscal, int numNota, int tipoPago, String fecha, String concepto, float cantidad, Producto productoPerecedero) {
        super(numNota, tipoPago, fecha, concepto, cantidad, productoPerecedero);
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domicilioFiscal = domicilioFiscal;
    }

    public Factura() {
        this.rfc = "";
        this.nombreCliente = "";
        this.domicilioFiscal = "";
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilioFiscal() {
        return domicilioFiscal;
    }

    public void setDomicilioFiscal(String domicilioFiscal) {
        this.domicilioFiscal = domicilioFiscal;
    }

    @Override
    public float calcularPago() {
        return this.productoPerecedero.calcularPrecio() * this.cantidad;
    }

    @Override
    public float calcularIva() {
        return this.calcularPago() * .16f;
    }

}
