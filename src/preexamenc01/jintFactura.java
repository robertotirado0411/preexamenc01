/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamenc01;

import javax.swing.JOptionPane;

/**
 *
 * @author Edgar Guerrero
 */
public class jintFactura extends javax.swing.JInternalFrame {

    /**
     * Creates new form jintFactura
     */
    public jintFactura() {
        initComponents();
        this.resize(881, 632);
        this.deshabilitar();
    }

    public void habilitar() {
        this.txtFecha.setEnabled(!false);
        this.txtCantidad.setEnabled(!false);
        this.txtConcepto.setEnabled(!false);
        this.txtID.setEnabled(!false);
        this.txtLote.setEnabled(!false);
        this.txtNomCli.setEnabled(!false);
        this.txtNomProd.setEnabled(!false);
        this.txtNumNota.setEnabled(!false);
        this.btnCancelar.setEnabled(!false);
        this.btnGuardar.setEnabled(!false);
        this.btnLimpiar.setEnabled(!false);
        this.btnMostrar.setEnabled(!false);
        this.chbPerecedero.setEnabled(!false);
        this.txtPrecio.setEnabled(!false);
        this.cmbTipo.setEnabled(!false);
        this.cmbUnidad.setEnabled(!false);
    }

    public void deshabilitar() {
        this.txtFecha.setEnabled(false);
        this.txtCantidad.setEnabled(false);
        this.txtConcepto.setEnabled(false);
        this.txtID.setEnabled(false);
        this.txtLote.setEnabled(false);
        this.txtNomCli.setEnabled(false);
        this.txtNomProd.setEnabled(false);
        this.txtNumNota.setEnabled(false);
        this.txtSubtotal.setEnabled(false);
        this.txtCaducidad.setEnabled(false);
        this.txtIVA.setEnabled(false);
        this.txtTotalAPagar.setEnabled(false);
        this.txtTemp.setEnabled(false);
        this.btnCancelar.setEnabled(false);
        this.btnGuardar.setEnabled(false);
        this.btnLimpiar.setEnabled(false);
        this.btnMostrar.setEnabled(false);
        this.chbPerecedero.setEnabled(false);
        this.txtPrecio.setEnabled(false);
        this.cmbTipo.setEnabled(false);
        this.cmbUnidad.setEnabled(false);
        this.chbPerecedero.setSelected(false);
    }

    public void limpiar() {
        this.txtFecha.setText("");
        this.txtCantidad.setText("");
        this.txtConcepto.setText("");
        this.txtID.setText("");
        this.txtLote.setText("");
        this.txtNomCli.setText("");
        this.txtNomProd.setText("");
        this.txtNumNota.setText("");
        this.txtSubtotal.setText("");
        this.txtCaducidad.setText("");
        this.txtIVA.setText("");
        this.txtTotalAPagar.setText("");
        this.txtTemp.setText("");
        this.txtPrecio.setText("");
        this.cmbTipo.setSelectedIndex(0);
        this.cmbUnidad.setSelectedIndex(0);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        txtFecha = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        txtConcepto = new javax.swing.JTextField();
        txtCantidad = new javax.swing.JTextField();
        txtNumNota = new javax.swing.JTextField();
        btnNuevo = new javax.swing.JButton();
        btnGuardar = new javax.swing.JButton();
        btnMostrar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnCerrar = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtNomCli = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        txtLote = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtTemp = new javax.swing.JTextField();
        txtCaducidad = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        txtID = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        txtNomProd = new javax.swing.JTextField();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtPrecio = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        chbPerecedero = new javax.swing.JCheckBox();
        cmbUnidad = new javax.swing.JComboBox<>();
        cmbTipo = new javax.swing.JComboBox<>();
        jPanel2 = new javax.swing.JPanel();
        jLabel18 = new javax.swing.JLabel();
        txtSubtotal = new javax.swing.JTextField();
        jLabel19 = new javax.swing.JLabel();
        txtIVA = new javax.swing.JTextField();
        txtTotalAPagar = new javax.swing.JTextField();
        jLabel20 = new javax.swing.JLabel();

        getContentPane().setLayout(null);

        jLabel1.setFont(new java.awt.Font("Arial", 1, 14)); // NOI18N
        jLabel1.setText("Fecha:");
        getContentPane().add(jLabel1);
        jLabel1.setBounds(90, 20, 100, 30);

        txtFecha.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        getContentPane().add(txtFecha);
        txtFecha.setBounds(140, 20, 180, 30);

        jLabel2.setText("Concepto:");
        getContentPane().add(jLabel2);
        jLabel2.setBounds(70, 160, 80, 30);

        jLabel3.setText("Cantidad:");
        getContentPane().add(jLabel3);
        jLabel3.setBounds(80, 200, 70, 30);

        jLabel5.setText("Num nota: ");
        getContentPane().add(jLabel5);
        jLabel5.setBounds(60, 70, 100, 30);

        txtConcepto.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        getContentPane().add(txtConcepto);
        txtConcepto.setBounds(150, 160, 180, 30);

        txtCantidad.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        getContentPane().add(txtCantidad);
        txtCantidad.setBounds(150, 200, 180, 30);

        txtNumNota.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        getContentPane().add(txtNumNota);
        txtNumNota.setBounds(150, 70, 180, 30);

        btnNuevo.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });
        getContentPane().add(btnNuevo);
        btnNuevo.setBounds(350, 30, 150, 80);

        btnGuardar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnGuardar.setText("Guardar");
        btnGuardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActionPerformed(evt);
            }
        });
        getContentPane().add(btnGuardar);
        btnGuardar.setBounds(520, 30, 150, 80);

        btnMostrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnMostrar.setText("Mostrar");
        btnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMostrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnMostrar);
        btnMostrar.setBounds(690, 30, 160, 80);

        btnLimpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnLimpiar.setText("Limpiar");
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });
        getContentPane().add(btnLimpiar);
        btnLimpiar.setBounds(350, 150, 150, 80);

        btnCancelar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCancelar);
        btnCancelar.setBounds(520, 150, 160, 80);

        btnCerrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });
        getContentPane().add(btnCerrar);
        btnCerrar.setBounds(690, 150, 160, 80);

        jLabel9.setText("Tipo de pago:");
        getContentPane().add(jLabel9);
        jLabel9.setBounds(50, 240, 100, 30);

        jLabel17.setText("Nombre del cliente:");
        getContentPane().add(jLabel17);
        jLabel17.setBounds(10, 120, 140, 30);

        txtNomCli.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        getContentPane().add(txtNomCli);
        txtNomCli.setBounds(150, 120, 180, 30);

        jPanel1.setBackground(new java.awt.Color(204, 204, 204));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Productos", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 14))); // NOI18N
        jPanel1.setLayout(null);

        txtLote.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtLote);
        txtLote.setBounds(180, 170, 170, 30);

        jLabel6.setText("Es perecedero");
        jPanel1.add(jLabel6);
        jLabel6.setBounds(20, 130, 120, 30);

        jLabel7.setText("Temperatura:");
        jPanel1.add(jLabel7);
        jLabel7.setBounds(50, 210, 100, 30);

        txtTemp.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtTemp);
        txtTemp.setBounds(180, 210, 170, 30);

        txtCaducidad.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtCaducidad);
        txtCaducidad.setBounds(180, 250, 170, 30);

        jLabel8.setText("Fecha de caducidad:");
        jPanel1.add(jLabel8);
        jLabel8.setBounds(10, 250, 150, 30);

        jLabel10.setText("Id:");
        jPanel1.add(jLabel10);
        jLabel10.setBounds(50, 20, 30, 30);

        txtID.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtID);
        txtID.setBounds(100, 20, 130, 30);

        jLabel11.setText("Nombre:");
        jPanel1.add(jLabel11);
        jLabel11.setBounds(20, 60, 70, 30);

        txtNomProd.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtNomProd);
        txtNomProd.setBounds(100, 60, 130, 30);

        jLabel12.setText("Unidad:");
        jPanel1.add(jLabel12);
        jLabel12.setBounds(270, 20, 60, 30);

        jLabel13.setText("Precio:");
        jPanel1.add(jLabel13);
        jLabel13.setBounds(270, 60, 50, 30);

        txtPrecio.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jPanel1.add(txtPrecio);
        txtPrecio.setBounds(340, 60, 130, 30);

        jLabel14.setText("Lote:");
        jPanel1.add(jLabel14);
        jLabel14.setBounds(110, 170, 40, 30);

        chbPerecedero.setBackground(new java.awt.Color(204, 204, 204));
        chbPerecedero.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        chbPerecedero.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                chbPerecederoActionPerformed(evt);
            }
        });
        jPanel1.add(chbPerecedero);
        chbPerecedero.setBounds(140, 120, 350, 30);

        cmbUnidad.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cmbUnidad.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un tipo", "Kilos", "Litros", "Pieza" }));
        cmbUnidad.setToolTipText("");
        cmbUnidad.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cmbUnidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbUnidadActionPerformed(evt);
            }
        });
        jPanel1.add(cmbUnidad);
        cmbUnidad.setBounds(350, 20, 130, 30);

        getContentPane().add(jPanel1);
        jPanel1.setBounds(10, 280, 510, 290);

        cmbTipo.setFont(new java.awt.Font("Arial", 0, 12)); // NOI18N
        cmbTipo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccione un tipo", "Contado", "Credito" }));
        cmbTipo.setToolTipText("");
        cmbTipo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        cmbTipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTipoActionPerformed(evt);
            }
        });
        getContentPane().add(cmbTipo);
        cmbTipo.setBounds(150, 240, 130, 30);

        jPanel2.setBackground(new java.awt.Color(153, 153, 153));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Pago", javax.swing.border.TitledBorder.LEFT, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 0, 16))); // NOI18N
        jPanel2.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jPanel2.setLayout(null);

        jLabel18.setBackground(new java.awt.Color(0, 0, 0));
        jLabel18.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel18.setText("Subtotal :");
        jPanel2.add(jLabel18);
        jLabel18.setBounds(40, 60, 80, 30);

        txtSubtotal.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtSubtotal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSubtotalActionPerformed(evt);
            }
        });
        jPanel2.add(txtSubtotal);
        txtSubtotal.setBounds(130, 60, 90, 30);

        jLabel19.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel19.setText("Imuestos :");
        jPanel2.add(jLabel19);
        jLabel19.setBounds(40, 20, 80, 30);

        txtIVA.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtIVA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtIVAActionPerformed(evt);
            }
        });
        jPanel2.add(txtIVA);
        txtIVA.setBounds(130, 20, 90, 30);

        txtTotalAPagar.setFont(new java.awt.Font("Arial", 0, 18)); // NOI18N
        txtTotalAPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTotalAPagarActionPerformed(evt);
            }
        });
        jPanel2.add(txtTotalAPagar);
        txtTotalAPagar.setBounds(130, 100, 90, 30);

        jLabel20.setFont(new java.awt.Font("Arial", 0, 16)); // NOI18N
        jLabel20.setText("Total a Pagar:");
        jPanel2.add(jLabel20);
        jLabel20.setBounds(10, 100, 120, 30);

        getContentPane().add(jPanel2);
        jPanel2.setBounds(540, 330, 290, 140);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        fac = new Factura();
        per = new Perecederos();
        nop = new NoPerecederos();
        this.habilitar();
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnGuardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActionPerformed
        // TODO add your handling code here:
        boolean exito = false;
        if (this.chbPerecedero.isSelected() == true) {
            if (this.txtCaducidad.getText().equals("")) {
                exito = true;
            }
            if (this.txtTemp.getText().equals("")) {
                exito = true;
            }
        } else {
            if (this.txtLote.getText().equals("")) {
                exito = true;
            }
        }
        if (this.txtCantidad.getText().equals("")) {
            exito = true;
        }
        if (this.txtConcepto.getText().equals("")) {
            exito = true;
        }
        if (this.txtFecha.getText().equals("")) {
            exito = true;
        }
        if (this.txtID.getText().equals("")) {
            exito = true;
        }
        if (this.txtNomCli.getText().equals("")) {
            exito = true;
        }
        if (this.txtNomProd.getText().equals("")) {
            exito = true;
        }
        if (this.cmbTipo.getSelectedIndex() == 0) {
            exito = true;
        }
        if (this.cmbUnidad.getSelectedIndex() == 0) {
            exito = true;
        }
        if (this.txtNumNota.getText().equals("")) {
            exito = true;
        }
        if (exito == true) {
            JOptionPane.showMessageDialog(this, "Falto capturar información");
        } else {
            try {
                fac.setNumNota(Integer.parseInt(this.txtNumNota.getText()));
                fac.setConcepto(this.txtConcepto.getText());
                fac.setFecha(this.txtFecha.getText());
                fac.setNombreCliente(this.txtNomCli.getText());
                fac.setTipoPago(this.cmbTipo.getSelectedIndex());
                fac.setCantidad(Float.parseFloat(this.txtCantidad.getText()));
                if (this.chbPerecedero.isSelected() == true) {
                    per.setFechaCaducidad(this.txtCaducidad.getText());
                    per.setTemperatura(this.txtTemp.getText());
                    per.setNombre(this.txtNomProd.getText());
                    per.setId(Integer.parseInt(this.txtID.getText()));
                    per.setUnidad(this.cmbUnidad.getSelectedIndex());
                    per.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
                    fac.setProductoPerecedero(per);
                } else {
                    nop.setNombre(this.txtNomProd.getText());
                    nop.setId(Integer.parseInt(this.txtID.getText()));
                    nop.setUnidad(this.cmbUnidad.getSelectedIndex());
                    nop.setLote(this.txtLote.getText());
                    nop.setPrecio(Float.parseFloat(this.txtPrecio.getText()));
                    fac.setProductoPerecedero(nop);
                }
            } catch (NumberFormatException e) {
                exito = true;
                JOptionPane.showMessageDialog(this, "Surgio un error " + e.getMessage());
                this.deshabilitar();
                this.limpiar();
            }
            if (exito == false) {
                this.btnMostrar.setEnabled(true);
                JOptionPane.showMessageDialog(this, "Se guardó la informacion con exito");
            }
        }
    }//GEN-LAST:event_btnGuardarActionPerformed

    private void btnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMostrarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.txtCantidad.setText(String.valueOf(fac.getCantidad()));
        this.txtConcepto.setText(fac.getConcepto());
        this.txtFecha.setText(fac.getFecha());
        this.txtNomCli.setText(fac.getNombreCliente());
        this.txtSubtotal.setText(String.valueOf(fac.calcularPago()));
        this.txtTotalAPagar.setText(String.valueOf(fac.calcularPago() + fac.calcularIva()));
        this.txtNumNota.setText(String.valueOf(fac.getNumNota()));
        this.txtIVA.setText(String.valueOf(fac.calcularIva()));
        this.cmbTipo.setSelectedIndex(fac.getTipoPago());
        if (this.chbPerecedero.isSelected() == true) {
            this.txtTemp.setText(per.getFechaCaducidad());
            this.txtCaducidad.setText(per.getFechaCaducidad());
            this.cmbUnidad.setSelectedIndex(per.getUnidad());
            this.txtPrecio.setText(String.valueOf(per.getPrecio()));
            this.txtID.setText(String.valueOf(per.getId()));
            this.txtNomProd.setText(per.getNombre());
        } else {
            this.cmbUnidad.setSelectedIndex(nop.getUnidad());
            this.txtPrecio.setText(String.valueOf(nop.getPrecio()));
            this.txtID.setText(String.valueOf(nop.getId()));
            this.txtLote.setText(nop.getLote());
            this.txtNomProd.setText(nop.getNombre());
        }
    }//GEN-LAST:event_btnMostrarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.limpiar();
        this.deshabilitar();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        // TODO add your handling code here:
        int op = 0;

        op = JOptionPane.showConfirmDialog(this, "¿Realmente quieres cerrar?", "Empleados", JOptionPane.YES_NO_OPTION);

        if (op == JOptionPane.YES_OPTION) {

            this.dispose();
        }
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void cmbUnidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbUnidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbUnidadActionPerformed

    private void txtSubtotalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSubtotalActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSubtotalActionPerformed

    private void txtIVAActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtIVAActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtIVAActionPerformed

    private void txtTotalAPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTotalAPagarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTotalAPagarActionPerformed

    private void cmbTipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTipoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTipoActionPerformed

    private void chbPerecederoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_chbPerecederoActionPerformed
        // TODO add your handling code here:

        if (chbPerecedero.isSelected() == true) {
            this.txtCaducidad.setEnabled(true);
            this.txtTemp.setEnabled(true);
            this.txtLote.setEnabled(!true);
            this.txtLote.setText("");
        } else {
            this.txtCaducidad.setEnabled(!true);
            this.txtTemp.setEnabled(!true);
            this.txtLote.setEnabled(true);
            this.txtTemp.setText("");
            this.txtCaducidad.setText("");
        }
    }//GEN-LAST:event_chbPerecederoActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnGuardar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnMostrar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JCheckBox chbPerecedero;
    private javax.swing.JComboBox<String> cmbTipo;
    private javax.swing.JComboBox<String> cmbUnidad;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JTextField txtCaducidad;
    private javax.swing.JTextField txtCantidad;
    private javax.swing.JTextField txtConcepto;
    private javax.swing.JTextField txtFecha;
    private javax.swing.JTextField txtID;
    private javax.swing.JTextField txtIVA;
    private javax.swing.JTextField txtLote;
    private javax.swing.JTextField txtNomCli;
    private javax.swing.JTextField txtNomProd;
    private javax.swing.JTextField txtNumNota;
    private javax.swing.JTextField txtPrecio;
    private javax.swing.JTextField txtSubtotal;
    private javax.swing.JTextField txtTemp;
    private javax.swing.JTextField txtTotalAPagar;
    // End of variables declaration//GEN-END:variables
    private Factura fac;
    private Perecederos per;
    private NoPerecederos nop;
}
