/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package preexamenc01;

/**
 *
 * @author Edgar Guerrero
 */
public abstract class Nota {

    protected int numNota, tipoPago;
    protected String fecha, concepto;
    protected float cantidad;
    protected Producto productoPerecedero;

    public Nota(int numNota, int tipoPago, String fecha, String concepto, float cantidad, Producto productoPerecedero) {
        this.numNota = numNota;
        this.tipoPago = tipoPago;
        this.fecha = fecha;
        this.concepto = concepto;
        this.cantidad = cantidad;
        this.productoPerecedero = productoPerecedero;
    }

    public Nota() {
        this.numNota = 0;
        this.tipoPago = 0;
        this.fecha = "";
        this.concepto = "";
        this.cantidad = 0.0f;
        this.productoPerecedero = new Perecederos();
    }

    public int getNumNota() {
        return numNota;
    }

    public void setNumNota(int numNota) {
        this.numNota = numNota;
    }

    public int getTipoPago() {
        return tipoPago;
    }

    public void setTipoPago(int tipoPago) {
        this.tipoPago = tipoPago;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public float getCantidad() {
        return cantidad;
    }

    public void setCantidad(float cantidad) {
        this.cantidad = cantidad;
    }

    public Producto getProductoPerecedero() {
        return productoPerecedero;
    }

    public void setProductoPerecedero(Producto productoPerecedero) {
        this.productoPerecedero = productoPerecedero;
    }

    public abstract float calcularPago();

}
